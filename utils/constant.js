// let BASE_URL = 'https://ngapi.coopaccess.xyz/api/';
// let BASE_URL = 'http://021f-180-252-174-50.ngrok.io/api/';
let BASE_URL = 'http://dev.coopaccess.co.id:1338/api/';
export const URL = {
  ROOT: 'http://dev.coopaccess.co.id:1338/api',
  LOGIN: `${BASE_URL}me/login`,
  REGISTER: `${BASE_URL}me/register`,
  MY_OUTLET: `${BASE_URL}me`,
  STORE: `${BASE_URL}my/stores`,
  PRODUCT: `${BASE_URL}my/products`,
  TOTAL_PRODUCT_SELLER: `${BASE_URL}my/count-product`,
  LIST_PRODUCT_SELLER: `${BASE_URL}my/products`,
  LIST_ORDER: `${BASE_URL}my/seller/transaction`,
  LIST_BUSINESS: `${BASE_URL}my/businesses`,
  LIST_COOP: `${BASE_URL}my/cooperative`,
  LIST_BANK: `${BASE_URL}my/banks`,
  BANK_ACCOUNTS: `${BASE_URL}my/bank-accounts`,
  MY_WALLET: `${BASE_URL}my/wallets`,
  LIST_OUTLET: `${BASE_URL}my/stores`,
};

export const String = {
  inputEmailOrPhone: 'Masukkan Email atau Nomor Ponsel',
  description: 'Deskripsi',
  emailOrPhone: 'Email atau Nomor Ponsel',
  email: 'Email',
  phoneNumber: 'Nomor Ponsel',
  fullName: 'Nama Lengkap',
  firstName: 'Nama Depan',
  lastName: 'Nama Belakang',
  login: 'Masuk',
  profile: 'Akun',
  myOutlet: 'Toko Saya',
  outletName: 'Nama Toko',
  domainName: 'Nama Domain',
  address: 'Alamat',
  regularMerchant: 'Regular Merchant',
  upgrade: 'Upgrade',
  transactionFromJoin: 'Transaksi Semenjak Bergabung',
  newTransaction: 'Pesanan Baru',
  register: 'Daftar',
  listOutlet: 'Daftar Toko',
  createOutlet: 'Buat Toko',
  back: 'Kembali',
  iHaveAlreadyAccount: 'Saya sudah memiliki akun',
  password: 'Kata Sandi',
  balance: 'Saldo',
  readyToSend: 'Siap Dikirim',
  product: 'Produk',
  addProduct: 'Tambah Produk',
  listProduct: 'Daftar Produkmu',
  detailOrder: 'Detail Pesanan',
  sellingList: 'Daftar Penjualan',
  listBusiness: 'Daftar Bisnis',
  review: 'Ulasan',
  discuss: 'Diskusi',
  orderComplaint: 'Pesanan Di Komplain',
  signInWithPhone: 'Masuk Dengan No. Hp',
  forgotPassword: 'Lupa Kata Sandi ?',
  save: 'Simpan',
};

export const Key = {
  jwtToken: 'JWT_TOKEN',
};

export const ScreenName = {
  profile: 'Profile',
  addBalance: 'Add_Balance',
  myOutlet: 'My_Outlet',
  register: 'Register',
  createOutlet: 'Create_Outlet',
  listOutlet: 'List_Outlet',
  listProduct: 'List_Product',
  withdraw: 'Withdraw',
  listBusiness: 'List_Business',
  addProduct: 'Add_Product',
  sellingList: 'Selling_List',
  balanceInfo: 'Balance_Info',
  detailOrder: 'Detail_Order',
  login: 'Login',
};

export const ProductStatus = {
  ACTIVE: 'ACTIVE',
  INACTIVE: 'INACTIVE',
};
