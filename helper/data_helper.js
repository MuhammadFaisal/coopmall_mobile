import AsyncStorage from '@react-native-async-storage/async-storage';
import {Key} from '../utils/constant';
import React from 'react';

export const getJwt = async () => {
  const value = await AsyncStorage.getItem(Key.jwtToken);
  if (value != null) {
    return value;
  }

  return '';
};

export default async function getJwtToken(): string {
  const value = await AsyncStorage.getItem(Key.jwtToken);
  if (value != null) {
    return value;
  }

  return '';
}
