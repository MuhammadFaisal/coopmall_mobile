export const featuredImageText = s => {
  let splitName = s.split(' ');
  let value = '';

  if (splitName.length > 1) {
    for (let i = 0; i < splitName.length; i++) {
      let data = splitName[i].charAt(0);

      if (i <= 2) {
        value += data;
      }
    }
  } else {
    value = splitName[0].charAt(0);
  }

  return value.toUpperCase();
};
export const bankCode = s => {
  if (s.length !== 3) {
    let data = s.substring(0, 3);

    return data.toUpperCase();
  }
  return s.toUpperCase();
};

export const currencyFormat = num => {
  if (num !== undefined) {
    return (
      'Rp' +
      parseFloat(num)
        .toFixed(0)
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    );
  }
  return 0;
};
