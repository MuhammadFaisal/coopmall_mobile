import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Key} from '../utils/constant';
import asyncStorage from '@react-native-async-storage/async-storage/src/AsyncStorage';
import {useEffect} from 'react';

const getJwtToken = async () => {
  asyncStorage.setItem('ok', 'ok');
};

export default function axiosHttpRequest(jwtToken) {
  return axios.create({
    timeout: 10000,
    headers: {
      Authorization: `Bearer ${jwtToken}`,
    },
  });
}
