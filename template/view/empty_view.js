import {Component} from 'react';
import {Image, Text, View} from 'react-native';
import React from 'react';
import font_style from '../../style/font_style';
import {color} from '../../style/color';

export class EmptyView extends Component {
  render() {
    return (
      <View style={{padding: 16, alignItems: 'center'}}>
        <Image
          source={require('../../images/vector/empty.png')}
          style={{resizeMode: 'contain', width: 256, height: 256}}
        />
        <Text style={[font_style.headerBold, color.black]}>Ups</Text>
        <Text style={[font_style.miniHeader, color.gray, {marginTop: 8}]}>Data tidak tersedia!</Text>
      </View>
    );
  }
}
