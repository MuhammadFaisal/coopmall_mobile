import {Image, Text, View} from 'react-native';
import styles from '../../style/styles';
import font_style from '../../style/font_style';
import React from 'react';
import {color, hexColor} from '../../style/color';
import RBSheet from 'react-native-raw-bottom-sheet';
import direction from '../../style/direction';
import {Button} from 'react-native-paper';
import Svg, {SvgUri} from 'react-native-svg';
import ErrSVG from '../../images/svg_vector/error.svg';

const ErrorBottomSheet = ({sheetRef, title, description, isUseHelpButton}) => {
  const [height, setHeight] = React.useState(0);
  const onLayout = event => {
    console.log(event.nativeEvent.layout);
    setHeight(event.nativeEvent.layout.height);
  };

  function onDismissButtonPressed() {
    sheetRef.current.close();
  }

  return (
    <RBSheet
      onLayout={onLayout}
      ref={sheetRef}
      closeOnDragDown={false}
      closeOnPressMask={true}
      height={400}
      customStyles={{container: {borderRadius: 16}}}>
      <View style={[styles.container, {alignItems: 'center'}]}>
        <Image
          source={require('../../images/vector/error.png')}
          style={{width: 256, height: 256}}
        />
        <Text style={[font_style.headerBold, color.black]}>
          {title !== undefined ? title : 'Terjadi Kesalahan'}
        </Text>
        <Text
          style={[
            font_style.normal,
            color.gray,
            {marginTop: 8, textAlign: 'center'},
          ]}>
          {description !== undefined ? description : 'Terjadi Kesalahan yang tidak diketahui. Harap coba kembali beberapa saat lagi.'}
        </Text>
        <View style={[direction.row, {marginTop: 16}]}>
          {isUseHelpButton && (
            <Button mode="outlined" style={{flex: 1, margin: 8}}>
              Bantuan
            </Button>
          )}
          <Button
            mode="contained"
            onPress={() => onDismissButtonPressed()}
            style={[styles.buttonPrimary, {flex: 1, marginHorizontal: 8}]}>
            Mengerti
          </Button>
        </View>
      </View>
    </RBSheet>
  );
};

export default ErrorBottomSheet;
