import RBSheet from 'react-native-raw-bottom-sheet';
import {ActivityIndicator, FlatList, View} from 'react-native';
import styles from '../../style/styles';
import {Divider, Text, TouchableRipple} from 'react-native-paper';
import font_style from '../../style/font_style';
import React, {useEffect, useMemo, useState} from 'react';
import {color, hexColor} from '../../style/color';
import direction from '../../style/direction';
import {bankCode} from '../../helper/general_helper';
import axios from 'axios';
import {URL} from '../../utils/constant';

const BankListBottomSheet = ({
  sheetRef,
  jwtToken,
  setServiceCode,
  setServiceName,
}) => {
  const [banks, setBanks] = useState([]);
  const [isLoading, setLoading] = useState(true);

  function onItemPressed(item) {
    console.log('addBank', item);
    setServiceCode(item.service_code);
    setServiceName(item.service_name);

    sheetRef.current.close();
  }

  useEffect(() => {
    const getListBank = async () => {
      axios
        .get(URL.LIST_BANK, {
          headers: {
            Authorization: `Bearer ${jwtToken}`,
          },
        })
        .then(response => {
          console.log('bankListBottomSheet()::then', response);
          setBanks(response.data.data);
          setLoading(false);

          return banks;
        })
        .catch(error => {
          alert('gagal mendapatkan data bank');

          return [];
        });
    };

    getListBank();
  }, []);

  const itemBanks = ({item}) => {
    console.log('itemBanks()', item);
    return (
      <TouchableRipple
        rippleColor={hexColor.gray}
        onPress={() => onItemPressed(item)}>
        <View style={[direction.row, {padding: 16, alignItems: 'center'}]}>
          <View
            style={{
              width: 64,
              height: 64,
              backgroundColor: hexColor.primary,
              borderRadius: 8,
              borderWidth: 1,
              borderColor: hexColor.primary,
              justifyContent: 'center',
            }}>
            <Text
              style={[
                font_style.headerBold,
                {alignSelf: 'center'},
                color.white,
              ]}>
              {bankCode(item.service_name)}
            </Text>
          </View>
          <View
            style={[
              direction.column,
              {marginHorizontal: 16, padding: 8, justifyContent: 'center'},
            ]}>
            <Text style={font_style.headerBold}>{item.service_name}</Text>
          </View>
        </View>
      </TouchableRipple>
    );
  };

  return (
    <RBSheet
      onClose={() => {}}
      ref={sheetRef}
      closeOnDragDown={true}
      closeOnPressMask={true}
      height={800}>
      <View style={[styles.container, direction.column]}>
        <View style={{marginHorizontal: 16, marginBottom: 24}}>
          <Text style={[font_style.biggerBold]}>Pilih Bank</Text>
        </View>
        {isLoading ? (
          <ActivityIndicator animating={true} color={hexColor.primary} />
        ) : (
          <FlatList
            data={banks}
            renderItem={itemBanks}
            ItemSeparatorComponent={() => <Divider />}
          />
        )}
      </View>
    </RBSheet>
  );
};

export default BankListBottomSheet;
