import RBSheet from 'react-native-raw-bottom-sheet';
import {FlatList, View} from 'react-native';
import styles from '../../style/styles';
import {useEffect} from 'react';
import React from 'react';
import {Divider, Text, TouchableRipple} from 'react-native-paper';
import direction from '../../style/direction';
import font_style from '../../style/font_style';
import {color, hexColor} from '../../style/color';
import {featuredImageText} from '../../helper/general_helper';
import RegisterFormScreen from '../../view/register/register_screen';

const CoopListBottomSheet = ({
  sheetRef,
  list,
  setParentCode,
  setParentName,
}) => {
  const [code, setCode] = React.useState('');
  const [name, setName] = React.useState('');

  const onLayout = event => {
    console.log(event.nativeEvent.layout);
  };

  useEffect(() => {
    console.log(list);
  }, [list]);

  function onItemPressed(item) {
    setParentName(item.name);
    setParentCode(item.code);

    sheetRef.current.close();
  }

  const itemCoopList = ({item}) => {
    return (
      <TouchableRipple
        rippleColor={hexColor.gray}
        onPress={() => onItemPressed(item)}>
        <View style={[direction.row, {padding: 16, alignItems: 'center'}]}>
          <View
            style={{
              width: 64,
              height: 64,
              backgroundColor: hexColor.primary,
              borderRadius: 8,
              borderWidth: 1,
              borderColor: hexColor.primary,
              justifyContent: 'center',
            }}>
            <Text
              style={[
                font_style.headerBold,
                {alignSelf: 'center'},
                color.white,
              ]}>
              {featuredImageText(item.name)}
            </Text>
          </View>
          <View
            style={[
              direction.column,
              {marginHorizontal: 16, padding: 8, justifyContent: 'center'},
            ]}>
            <Text style={font_style.headerBold}>{item.name}</Text>
            <Text style={[font_style.normal, {marginTop: 8}]}>
              Kode Koperasi: {item.code}
            </Text>
          </View>
        </View>
      </TouchableRipple>
    );
  };

  return (
    <RBSheet
      onClose={() => {}}
      ref={sheetRef}
      closeOnDragDown={true}
      closeOnPressMask={true}
      onLayout={onLayout}
      height={800}>
      <View style={[styles.container]}>
        <View style={{marginHorizontal: 16, marginBottom: 24}}>
          <Text style={[font_style.biggerBold]}>Pilih Koperasi</Text>
          <Text style={[font_style.normal, {marginTop: 8}]}>
            Berasal dari koperasi mana anda?
          </Text>
        </View>
        <FlatList
          data={list}
          renderItem={itemCoopList}
          ItemSeparatorComponent={() => <Divider />}
        />
      </View>
    </RBSheet>
  );
};

export default CoopListBottomSheet;
