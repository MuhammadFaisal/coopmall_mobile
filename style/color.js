import {StyleSheet} from 'react-native';

export const color = StyleSheet.create({
  primary: {
    color: '#CE1212',
  },
  darkPrimary: {
    color: '#810000',
  },
  white: {
    color: 'white',
  },
  black: {
    color: '#1B1717',
  },
  gray: {
    color: '#8e8e8e',
  },

  accent: {
    color: '#EEEBDD',
  },
});

export const hexColor = {
  primary: '#CE1212',
  primary30: 'rgba(206,18,18,0.3)',
  lightPrimary: '#ffa7a2',
  lightPrimary30: 'rgba(255,167,162,0.3)',
  darkPrimary: '#810000',
  accent: '#EEEBDD',
  white: '#FFFFFF',
  black: '#1B1717',
  darkGray: '#595050',
  gray: '#8e8e8e',
  softGray: '#EAEAEA',
};
