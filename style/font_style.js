import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  bigger: {
    fontSize: 24,
  },
  biggerBold: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  header: {
    fontSize: 18,
  },
  headerBold: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  miniHeader: {
    fontSize: 16,
  },
  miniHeaderBold: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 12,
    fontWeight: 'normal',
  },
  captionBold: {
    fontSize: 12,
    fontWeight: 'bold',
  },
  normal: {
    fontSize: 14,
    fontWeight: 'normal',
  },
  normalBold: {
    fontSize: 14,
    fontWeight: 'bold',
  },
});
