import {StyleSheet} from 'react-native';
import {hexColor} from './color';

export default StyleSheet.create({
  container: {
    padding: 8,
  },
  buttonPrimary: {
    backgroundColor: hexColor.primary,
    borderRadius: 8,
  },
});
