import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import LoginScreen from '../view/login/login_screen';
import RegisterFormScreen from '../view/register/register_screen';
import LinearGradient from 'react-native-linear-gradient';
import React from 'react';
import 'react-native-gesture-handler';
import {color, hexColor} from '../style/color';
import {ScreenName, String} from '../utils/constant';
import CreateOutletScreen from '../view/outlet/register/create_outlet_screen';
import MyOutletScreen from '../view/outlet/dashboard/my_outlet_screen';
import ListOutletScreen from '../view/outlet/list/list_outlet_screen';
import ListProductScreen from '../view/outlet/list/list_product_screen';
import AddProductScreen from '../view/outlet/form/add_product_screen';
import {Button, IconButton, TouchableRipple} from 'react-native-paper';
import ListBusinessScreen from '../view/business/list_business_screen';
import SellingListScreen from '../view/outlet/list/selling_list_screen';
import DetailOrderScreen from '../view/outlet/order/detail_order_screen';
import BalanceInfoScreen from '../view/outlet/balance/balance_info_screen';
import WithdrawScreen from '../view/outlet/balance/withdraw_screen';
import AddBalanceAccountScreen from '../view/outlet/balance/add_balance_account_screen';

const Stack = createStackNavigator();

export default function index() {
  let initialRouteName = ScreenName.login;

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={initialRouteName}>
        <Stack.Screen
          name={ScreenName.login}
          component={LoginScreen}
          options={{
            headerShown: false,
            headerBackground: () => gradientHeaderOptions(),
            headerTintColor: `${hexColor.white}`,
            headerTitle: `${String.login}`,
            headerTitleStyle: {color: hexColor.white},
          }}
        />
        <Stack.Screen
          name={ScreenName.myOutlet}
          component={MyOutletScreen}
          options={{
            headerShown: false,
            headerBackground: () => whiteHeaderOptions(),
            headerTintColor: `${hexColor.primary}`,
            headerTitle: `${String.myOutlet}`,
            headerTitleStyle: {color: hexColor.primary},
          }}
        />
        <Stack.Screen
          name={ScreenName.register}
          component={RegisterFormScreen}
          options={{
            headerTitle: `${String.register}`,
            headerBackground: () => gradientHeaderOptions(),
            headerTitleStyle: {color: hexColor.white},
          }}
        />
        <Stack.Screen
          name={ScreenName.createOutlet}
          component={CreateOutletScreen}
          options={{
            headerTitle: `${String.register}`,
            headerBackground: () => gradientHeaderOptions(),
            headerTitleStyle: {color: hexColor.white},
          }}
        />
        <Stack.Screen
          name={ScreenName.addProduct}
          component={AddProductScreen}
          options={{
            headerTitle: `${String.addProduct}`,
            headerBackground: () => gradientHeaderOptions(),
            headerTitleStyle: {color: hexColor.white},
            headerTintColor: hexColor.white,
          }}
        />
        <Stack.Screen
          name={ScreenName.listProduct}
          component={ListProductScreen}
          options={({navigation}) => ({
            headerTitle: `${String.listProduct}`,
            headerBackground: () => gradientHeaderOptions(),
            headerTitleStyle: {color: hexColor.white},
            headerTintColor: hexColor.white,
            headerRight: () => (
              <IconButton
                icon={require('../images/icon/ic_add.png')}
                onPress={() =>
                  navigation.push(ScreenName.addProduct, {productId: ''})
                }
              />
            ),
          })}
        />
        <Stack.Screen
          name={ScreenName.withdraw}
          component={WithdrawScreen}
          options={({navigation}) => ({
            headerTitle: 'Cairkan Saldo',
            headerTintColor: hexColor.white,
            headerBackground: () => gradientHeaderOptions(),
            headerTitleStyle: {color: hexColor.white},
          })}
        />
        <Stack.Screen
          name={ScreenName.addBalance}
          component={AddBalanceAccountScreen}
          options={({navigation}) => ({
            headerTitle: 'Tambah Rekening',
            headerTintColor: hexColor.white,
            headerBackground: () => gradientHeaderOptions(),
            headerTitleStyle: {color: hexColor.white},
          })}
        />
        <Stack.Screen
          name={ScreenName.balanceInfo}
          component={BalanceInfoScreen}
          options={() => ({
            headerTitle: 'Saldo Penjualaan',
            headerTintColor: hexColor.white,
            headerBackground: () => gradientHeaderOptions(),
            headerTitleStyle: {color: hexColor.white},
          })}
        />
        <Stack.Screen
          name={ScreenName.sellingList}
          component={SellingListScreen}
          options={({navigation}) => {
            return {
              headerTitle: 'Daftar Pesanan',
              headerBackground: () => gradientHeaderOptions(),
              headerTitleStyle: {color: hexColor.white},
              headerTintColor: hexColor.white,
            };
          }}
        />
        <Stack.Screen
          name={ScreenName.detailOrder}
          component={DetailOrderScreen}
          options={({navigation}) => ({
            headerTitle: 'Detail Pesanan',
            headerTintColor: hexColor.white,
            headerBackground: () => gradientHeaderOptions(),
            headerTitleStyle: {color: hexColor.white},
          })}
        />
        <Stack.Screen
          name={ScreenName.listBusiness}
          component={ListBusinessScreen}
          options={({navigation}) => ({
            headerTitle: `${String.listBusiness}`,
            headerBackground: () => gradientHeaderOptions(),
            headerTitleStyle: {color: hexColor.white},
          })}
        />
        <Stack.Screen
          name={ScreenName.listOutlet}
          component={ListOutletScreen}
          options={{
            headerTitle: `${String.listOutlet}`,
            headerBackground: () => gradientHeaderOptions(),
            headerTitleStyle: {color: hexColor.white},
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

function gradientHeaderOptions() {
  return (
    <LinearGradient
      colors={[hexColor.primary, hexColor.darkPrimary]}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      style={{flex: 1}}
    />
  );
}

function whiteHeaderOptions() {
  return (
    <LinearGradient
      colors={[hexColor.white, hexColor.white]}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      style={{flex: 1}}
    />
  );
}
