import {
  FlatList,
  Image,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  ActivityIndicator,
  Avatar,
  Button,
  Divider,
  Text,
  TouchableRipple,
} from 'react-native-paper';
import React, {useEffect, useRef, useState} from 'react';
import getJwtToken from '../../../helper/data_helper';
import {ScreenName, String, URL} from '../../../utils/constant';
import direction from '../../../style/direction';
import font_style from '../../../style/font_style';
import {color, hexColor} from '../../../style/color';
import axios from 'axios';
import ErrorBottomSheet from '../../../template/bottomsheet/error_bottom_sheet';
import DashedLine from 'react-native-dashed-line';
import {
  currencyFormat,
  featuredImageText,
} from '../../../helper/general_helper';

export default function MyOutletScreen({navigation}) {
  const [jwtToken, setJwtToken] = React.useState('');
  const [name, setName] = React.useState('');
  const [totalProduct, setTotalProduct] = React.useState(0);
  const [products, setProducts] = React.useState([]);
  const [isProductLoading, setProductLoading] = React.useState(true);
  const [isRefresh, setRefresh] = useState(false);

  const errSheetRef = useRef();
  const [errorTitle, setErrorTitle] = React.useState('Terjadi Kesalahan');
  const [errorDescription, setErrorDescription] = React.useState('');

  let role = [];

  const getDataProfile = async () => {
    require('axios-debug-log');
    if (jwtToken !== '') {
      axios
        .get(URL.STORE, {headers: {authorization: 'Bearer ' + jwtToken}})
        .then(function (response) {
          console.log('getDataProfile()::then', response.data);
          let storeResponse = response.data.data[0];
          setName(storeResponse.name);
        })
        .catch(function (response) {
          console.log('getDataProfile()::catch', response);
          setErrorDescription(
            'Terjadi kesalahan saat mengambil data toko. Mohon coba kembali beberapa saat lagi',
          );
          errSheetRef.current.open();
        });
    }
  };

  const getTotalProduct = async () => {
    axios
      .get(URL.TOTAL_PRODUCT_SELLER, {
        headers: {Authorization: `Bearer ${jwtToken}`},
      })
      .then(function (response) {
        setTotalProduct(response.data.count);
        setRefresh(false);
      })
      .catch(function (response) {
        console.log(response);
        setErrorDescription(
          'Terjadi kesalahan saat mengambil data total produk. Mohon coba kembali beberapa saat lagi',
        );
        errSheetRef.current.open();
      });
  };

  const getProducts = async () => {
    if (jwtToken !== '') {
      axios
        .get(URL.LIST_PRODUCT_SELLER, {
          headers: {Authorization: `Bearer ${jwtToken}`},
        })
        .then(function (response) {
          let responseData = response.data;

          if (responseData.code === 200) {
            setProducts(responseData.data);
          } else {
            errSheetRef.current.open();
            setErrorTitle('Terjadi Kesalahan');
            setErrorDescription(responseData.error.message);
          }
          setProductLoading(false);
          console.log('getProducts::then() ', response.data);
        })
        .catch(function (response) {
          errSheetRef.current.open();
          setErrorTitle('Terjadi Kesalahan');
          setErrorDescription(
            'Terjadi kesalahan saat mengambil data produk. Mohon coba kembali beberapa saat lagi',
          );
          console.log('getProducts::catch()', response);
        });
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      const data = await getJwtToken();
      setJwtToken(data);
    };

    fetchData();
    getDataProfile();
    getTotalProduct();
    getProducts();
  }, [jwtToken]);

  function _onListProductPressed() {
    console.log('onListProductPressed();');
    navigation.push(ScreenName.addProduct, {productId: ''});
  }

  function _onNewOrderPressed() {
    console.log('onNewOrderPressed();');
    navigation.push(ScreenName.sellingList, {status: 'Pesanan Baru'});
  }
  const productRender = ({item}) => {
    // console.log('productRender()', item);
    let featured_image = item.featured_image;
    let endpoint = '';
    if (featured_image != null) {
      endpoint = featured_image.formats.thumbnail.url;
    }
    return (
      <TouchableOpacity onPress={() => _onProductPressed(item)}>
        <View style={[direction.row, padding(8).all]}>
          {endpoint !== '' ? (
            <Image
              source={{uri: `http://dev.coopaccess.co.id:1338${endpoint}`}}
              style={{width: 56, height: 56, resizeMode: 'contain'}}
            />
          ) : (
            <View
              style={{
                width: 64,
                height: 64,
                backgroundColor: hexColor.primary,
                borderRadius: 8,
                borderWidth: 1,
                borderColor: hexColor.primary,
                justifyContent: 'center',
              }}>
              <Text
                style={[
                  font_style.headerBold,
                  {alignSelf: 'center'},
                  color.white,
                ]}>
                {featuredImageText(item.name)}
              </Text>
            </View>
          )}
          <View
            style={[direction.column, margin(16).horizontal, {flexShrink: 1}]}>
            <Text style={[font_style.normalBold]}>{item.name}</Text>
            <Text style={font_style.normal}>
              {currencyFormat(item.catalogs[0].price)}
            </Text>
            <Text style={[font_style.caption, margin(8).top, color.gray]}>
              Stok {item.catalogs[0].sku}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  function _onProductPressed(item) {
    console.log(
      'listProductScreen::onPressed()',
      `Push Product Id ${item.id} to Detail Product Screen.`,
    );
    navigation.push(ScreenName.addProduct, {
      productId: item.id,
    });
  }

  function onRefresh() {
    setRefresh(true);
    getDataProfile();
    getTotalProduct();
    getProducts();
  }

  function _onOrderProcessPressed() {
    console.log('onOrderProcessPressed();');
    navigation.push(ScreenName.sellingList, {status: 'Pesanan Diproses'});
  }

  function _onHistoryPressed() {
    console.log('onHistoryPressed()');
    navigation.push(ScreenName.sellingList, {status: ''});
  }

  return (
    <>
      <SafeAreaView>
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={isRefresh} onRefresh={onRefresh} />
          }>
          <MerchantInfo accountName={name} navigation={navigation} />
          <MerchantSales
            totalProduct={totalProduct}
            onListProductPressed={_onListProductPressed}
            onNewOrderPressed={_onNewOrderPressed}
            onOrderProcessPressed={_onOrderProcessPressed}
            onHistoryPressed={_onHistoryPressed}
          />
          {isProductLoading && (
            <ActivityIndicator
              animating={true}
              color={hexColor.primary}
              style={{margin: 24}}
            />
          )}
          <FlatList
            style={[margin(4).top, {backgroundColor: hexColor.white}]}
            data={products}
            renderItem={productRender}
            ItemSeparatorComponent={() => (
              <Divider style={margin(8).vertical} />
            )}
            keyExtractor={item => item.id}
          />

          <ErrorBottomSheet
            sheetRef={errSheetRef}
            title={errorTitle}
            description={errorDescription}
            isUseHelpButton={false}
          />
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const MerchantInfo = ({accountName, navigation}) => (
  <View style={[direction.column, {backgroundColor: 'white'}]}>
    <View
      style={[
        direction.row,
        margin(16).all,
        {justifyContent: 'space-between'},
      ]}>
      <View style={direction.row}>
        <View
          style={{
            width: 64,
            height: 64,
            backgroundColor: hexColor.darkPrimary,
            borderRadius: 8,
            borderWidth: 1,
            borderColor: hexColor.darkPrimary,
            justifyContent: 'center',
          }}>
          <Text
            style={[font_style.headerBold, {alignSelf: 'center'}, color.white]}>
            {featuredImageText(accountName)}
          </Text>
        </View>
        <View
          style={[
            direction.column,
            margin(16).horizontal,
            {justifyContent: 'center'},
          ]}>
          <Text style={font_style.headerBold}>{accountName}</Text>
        </View>
      </View>
      <Button
        style={{alignSelf: 'center'}}
        color={hexColor.darkGray}
        icon="bell">
        0
      </Button>
    </View>
    <View
      style={[
        margin(16).horizontal,
        padding(12).all,
        {
          borderRadius: 8,
          borderWidth: 1,
          borderColor: hexColor.primary,
          backgroundColor: hexColor.primary,
        },
      ]}>
      <View style={[direction.row, {justifyContent: 'space-between'}]}>
        <Text style={[font_style.normal, color.white]}>
          {String.regularMerchant}
        </Text>
        <TouchableRipple onPress={() => alert('Ok')}>
          <Text style={[font_style.normalBold, color.white]}>
            {String.upgrade}
          </Text>
        </TouchableRipple>
      </View>

      <View
        style={[
          direction.row,
          {justifyContent: 'space-between'},
          margin(16).top,
        ]}>
        <Text style={[font_style.normal, color.white]}>
          {String.transactionFromJoin}
        </Text>
        <Text style={[font_style.normalBold, color.white]}>0/100</Text>
      </View>
    </View>

    <View style={[direction.row, {justifyContent: 'space-between'}]}>
      <Text style={[padding(16).all, font_style.normal, color.black]}>
        {String.balance}
      </Text>
      <TouchableRipple onPress={() => navigation.push(ScreenName.balanceInfo)}>
        <Text style={[padding(16).all, font_style.normalBold, color.black]}>
          Rp0
        </Text>
      </TouchableRipple>
    </View>
  </View>
);

const MerchantSales = ({
  totalProduct,
  onListProductPressed,
  onNewOrderPressed,
  onOrderProcessPressed,
  onHistoryPressed,
}) => (
  <View
    style={[
      direction.column,
      {padding: 16, backgroundColor: 'white'},
      margin(8).top,
    ]}>
    <View style={[direction.row, {justifyContent: 'space-between'}]}>
      <Text style={[font_style.normalBold, color.gray]}>Penjualan</Text>
      <TouchableRipple onPress={onHistoryPressed}>
        <Text style={[font_style.captionBold, color.primary]}>
          Lihat Riwayat
        </Text>
      </TouchableRipple>
    </View>
    <View
      style={[
        direction.row,
        {justifyContent: 'space-around'},
        margin(8).horizontal,
        margin(24).top,
      ]}>
      <TouchableRipple onPress={onNewOrderPressed}>
        <View
          style={[
            direction.row,
            {justifyContent: 'center', alignItems: 'center', display: 'flex'},
          ]}>
          <Avatar.Icon
            icon="basket"
            size={32}
            style={{backgroundColor: hexColor.primary}}
          />
          <Text style={[font_style.normalBold, color.black, margin(8).start]}>
            {String.newTransaction}
          </Text>
        </View>
      </TouchableRipple>
      <TouchableRipple onPress={onOrderProcessPressed}>
        <View
          style={[
            direction.row,
            {justifyContent: 'center', alignItems: 'center', display: 'flex'},
          ]}>
          <Avatar.Icon
            icon="package"
            size={32}
            style={{backgroundColor: hexColor.primary}}
          />
          <Text style={[font_style.normalBold, color.black, margin(8).start]}>
            Pesanan Diproses
          </Text>
        </View>
      </TouchableRipple>
    </View>
    <Divider style={margin(16).all} />
    <View style={[direction.row, {justifyContent: 'space-between'}]}>
      <View style={direction.column}>
        <Text style={font_style.miniHeaderBold}>{String.listProduct}</Text>
        <Text style={[font_style.caption, margin(8).top]}>{totalProduct}</Text>
      </View>
      <Button
        mode="outlined"
        onPress={onListProductPressed}
        color={hexColor.primary}
        style={{alignSelf: 'center'}}>
        Tambah Produk
      </Button>
    </View>

    {/*<View style={direction.column}>
      <View style={[direction.row, margin(8).vertical, {alignItems: 'center'}]}>
        <Image
          style={[
            margin(8).end,
            {width: 12, height: 12, resizeMode: 'contain'},
          ]}
          source={require('../../../images/icon/ic_simple_arrow_next.png')}
        />
        <Text>{String.review}</Text>
      </View>
      <View style={[direction.row, margin(8).vertical, {alignItems: 'center'}]}>
        <Image
          style={[
            margin(8).end,
            {width: 12, height: 12, resizeMode: 'contain'},
          ]}
          source={require('../../../images/icon/ic_simple_arrow_next.png')}
        />
        <Text>{String.discuss}</Text>
      </View>
      <View style={[direction.row, margin(8).vertical, {alignItems: 'center'}]}>
        <Image
          style={[
            margin(8).end,
            {width: 12, height: 12, resizeMode: 'contain'},
          ]}
          source={require('../../../images/icon/ic_simple_arrow_next.png')}
        />
        <Text>{String.orderComplaint}</Text>
      </View>
    </View>*/}
  </View>
);

const margin = (v?: number) =>
  StyleSheet.create({
    all: {
      margin: v,
    },
    start: {
      marginStart: v,
    },
    end: {
      marginEnd: v,
    },
    top: {
      marginTop: v,
    },
    vertical: {
      marginTop: v,
      marginBottom: v,
    },
    horizontal: {
      marginStart: v,
      marginEnd: v,
    },
  });

const padding = (v?: number) =>
  StyleSheet.create({
    start: {
      paddingStart: v,
    },
    end: {
      paddingEnd: v,
    },
    top: {
      paddingTop: v,
    },

    bottom: {
      paddingBottom: v,
    },

    horizontal: {
      paddingBottom: v,
    },

    vertical: {
      paddingVertical: v,
    },

    all: {
      padding: v,
    },
  });
