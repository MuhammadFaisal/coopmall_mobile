import {FlatList, SafeAreaView, Text, View} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import font_style from '../../../style/font_style';
import {ActivityIndicator, Button} from 'react-native-paper';
import {color, hexColor} from '../../../style/color';
import {EmptyView} from '../../../template/view/empty_view';
import {ScreenName} from '../../../utils/constant';
import axiosHttpRequest from '../../../api/api';
import getJwtToken from '../../../helper/data_helper';
import ErrorBottomSheet from '../../../template/bottomsheet/error_bottom_sheet';
import {URL} from '../../../utils/constant';
import {currencyFormat} from '../../../helper/general_helper';

export default function BalanceInfoScreen({route, navigation}) {
  const [isLoading, setLoading] = React.useState(true);

  const [jwtToken, setJwtToken] = useState('');
  const [start, setStart] = useState('');
  const [limit, setLimit] = useState('');

  const [mutation, setMutation] = useState([]);
  const [wallet, setWallet] = useState({});

  const sheetRef = useRef();
  const [error, setError] = useState('');

  useEffect(() => {
    const fetchJwtToken = async () => {
      const value = await getJwtToken();
      setJwtToken(value);
    };

    const getData = async () => {
      if (jwtToken !== '') {
        await axiosHttpRequest(jwtToken)
          .get(URL.MY_WALLET)
          .then(function (response) {
            setLoading(false);
            let data = response.data;
            console.log('balanceInfoScreen()::then', data);

            if (data.code === 200) {
              setMutation(data.wallet_history);
              setWallet(data.wallet);
            } else {
              //Handle Error
              setError(data.message);
              sheetRef.current.open();
            }
          })
          .catch(function (error) {
            setLoading(false);
            setError(error.message);

            sheetRef.current.open();
          });
      }
    };

    fetchJwtToken();
    getData();
  }, [jwtToken]);

  function itemBalanceHistory() {
    return <Text>Ok</Text>;
  }

  function onButtonWithdrawPressed() {
    if (!isLoading) {
      navigation.navigate(ScreenName.withdraw, {balance: wallet.balance});
    }
  }

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: hexColor.softGray}}>
      <View style={{backgroundColor: hexColor.white, padding: 16}}>
        <Text style={font_style.normalBold}>Saldo</Text>
        <Text style={[font_style.headerBold, color.black]}>
          {wallet.balance !== undefined ? currencyFormat(wallet.balance) : 0}
        </Text>
        <Button
          mode="contained"
          onPress={() => onButtonWithdrawPressed()}
          style={{marginTop: 16, backgroundColor: hexColor.primary}}>
          Cairkan Saldo
        </Button>
      </View>
      <View style={{flex: 1, backgroundColor: hexColor.white, marginTop: 8}}>
        <EmptyView />
        <ActivityIndicator
          animating={false}
          color={hexColor.primary}
          size="large"
        />
        <FlatList data={mutation} renderItem={itemBalanceHistory()} />
      </View>
      <ErrorBottomSheet sheetRef={sheetRef} description={error} />
    </SafeAreaView>
  );
}
