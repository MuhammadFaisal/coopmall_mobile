import {
  ActivityIndicator,
  FlatList,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import {Button, Divider, TextInput, TouchableRipple} from 'react-native-paper';
import {color, hexColor} from '../../../style/color';
import font_style from '../../../style/font_style';
import direction from '../../../style/direction';
import {ScreenName} from '../../../utils/constant';
import axiosHttpRequest from '../../../api/api';
import {URL} from '../../../utils/constant';
import getJwtToken from '../../../helper/data_helper';
import axios from 'axios';
import {bankCode, currencyFormat} from '../../../helper/general_helper';
import SelectableFlatlist, {STATE} from 'react-native-selectable-flatlist';
import ErrorBottomSheet from '../../../template/bottomsheet/error_bottom_sheet';

export default function WithdrawScreen({route, navigation}) {
  const {balance} = route.params;
  const [amount, setAmount] = React.useState('');
  const [accounts, setAccounts] = React.useState([]);

  const [accountSelected, setAccountSelected] = useState('');

  const [isLoading, setLoading] = React.useState(true);

  const [jwtToken, setJwtToken] = useState('');
  const [error, setError] = useState('');
  const errSheetRef = useRef();

  useEffect(() => {
    const fetchJwtToken = async () => {
      const jwt = await getJwtToken();
      setJwtToken(jwt);
    };

    const getListBankAccounts = async () => {
      console.log('withdrawScreen()::getListBankAccounts', 'func started();');

      axiosHttpRequest(jwtToken)
        .get(URL.BANK_ACCOUNTS)
        .then(function (response) {
          setLoading(false);
          let data = response.data;
          console.log('withdrawScreen()::then', data);

          if (data.code === 200) {
            setAccounts(data.data);
          } else {
            setError(`${data.message}. [${data.code}]`);
            errSheetRef.current.open();
          }
        })
        .catch(function (error) {
          setError(error.message);
          errSheetRef.current.open();
        });
    };

    fetchJwtToken();
    getListBankAccounts();
  }, [jwtToken]);

  function onItemPressed(item) {
    console.log(item);
    if (accountSelected !== item.id) {
      setAccountSelected(item.id);
    } else {
      setAccountSelected('');
    }
  }

  const accountRenderItem = ({item}) => {
    return (
      <TouchableOpacity onPress={() => onItemPressed(item)}>
        <View
          style={[
            item.id === accountSelected ? items().selected : {},
            direction.row,
            {alignItems: 'center', padding: 8},
          ]}>
          <View style={[direction.row, {alignItems: 'center'}]}>
            <View
              style={{
                width: 64,
                height: 64,
                backgroundColor: hexColor.primary,
                borderRadius: 8,
                borderWidth: 1,
                borderColor: hexColor.primary,
                justifyContent: 'center',
              }}>
              <Text
                style={[
                  font_style.headerBold,
                  {alignSelf: 'center'},
                  color.white,
                ]}>
                {bankCode(item.service_name)}
              </Text>
            </View>
          </View>
          <View style={[direction.column, {marginStart: 16}]}>
            <Text style={[font_style.miniHeaderBold, color.black]}>
              [{item.service_name}] {item.account_number}
            </Text>
            <Text style={[font_style.normal, color.black, {marginTop: 8}]}>
              {item.account_name}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  function onButtonAddAccountPressed() {
    navigation.navigate(ScreenName.addBalance);
  }

  function processWithdraw() {
    console.log('processing withdrawal.');
  }

  function onNext() {
    let currentBalance = parseFloat(balance);
    let withdrawAmount = parseFloat(amount);

    if (currentBalance > withdrawAmount) {
      if (withdrawAmount < 10) {
        setError('Minimum penarikan saldo adalah Rp50.000');
        errSheetRef.current.open();
        return;
      }

      processWithdraw();
    } else {
      setError(
        'Saldo anda tidak cukup untuk melakukan penarikkan saldo. Pastikan anda telah memasukkan nominal lebih kecil atau sama dengan saldo tersedia anda',
      );
      errSheetRef.current.open();
    }
  }

  return (
    <SafeAreaView
      style={{padding: 16, flex: 1, backgroundColor: hexColor.white}}>
      <TextInput
        label="Saldo"
        value={currencyFormat(balance).toString().replace('Rp', '')}
        disabled={true}
        mode="outlined"
        left={<TextInput.Affix animate={false} text="Rp" />}
        activeOutlineColor={hexColor.primary}
        onChangeText={text => setAmount(text)}
      />
      <TextInput
        mode="outlined"
        activeOutlineColor={hexColor.primary}
        style={{marginTop: 16}}
        label="Jumlah Penarikan"
        value={amount}
        left={<TextInput.Affix animate={false} text="Rp" />}
        onChangeText={text => setAmount(text)}
      />
      <Text style={[font_style.miniHeaderBold, color.black, {marginTop: 24}]}>
        Rekening Tujuan
      </Text>
      <View style={[direction.column, {marginTop: 16}]}>
        {isLoading && (
          <ActivityIndicator animating={true} color={hexColor.primary} />
        )}
        <FlatList
          data={accounts}
          extraData={accountSelected}
          renderItem={accountRenderItem}
          keyExtractor={account => account.id.toString()}
          ItemSeparatorComponent={() => <Divider />}
        />
        <Button
          onPress={() => onButtonAddAccountPressed()}
          mode="outlined"
          color={hexColor.primary}
          style={{marginTop: 16}}>
          Tambah Rekening Tujuan
        </Button>
      </View>
      <Button
        onPress={() => onNext()}
        mode="contained"
        labelStyle={color.white}
        color={hexColor.primary}
        style={{bottom: 0, position: 'absolute', width: '100%', margin: 16}}>
        Selanjutnya
      </Button>

      <ErrorBottomSheet sheetRef={errSheetRef} description={error} />
    </SafeAreaView>
  );
}

const items = () =>
  StyleSheet.create({
    selected: {
      backgroundColor: hexColor.lightPrimary30,
      borderRadius: 8,
      borderColor: hexColor.darkPrimary,
      borderWidth: 1,
    },
  });
