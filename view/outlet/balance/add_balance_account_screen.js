import React, {useEffect, useRef, useState} from 'react';
import {Button, TextInput} from 'react-native-paper';
import {hexColor} from '../../../style/color';
import {ActivityIndicator, SafeAreaView} from 'react-native';
import BankListBottomSheet from '../../../template/bottomsheet/bank_list_bottom_sheet';
import axios, {Axios} from 'axios';
import {ScreenName, URL} from '../../../utils/constant';
import getJwtToken from '../../../helper/data_helper';
import axiosHttpRequest, {axiosRequest, InternalHttpClient} from '../../../api/api';

export default function AddBalanceAccountScreen({route, navigation}) {
  const [jwtToken, setJwtToken] = useState('');

  const [name, setName] = React.useState('');
  const [bank, setBank] = React.useState('');
  const [number, setNumber] = React.useState('');

  const [error, setError] = useState('');
  const [errDesc, setErrDesc] = useState('');

  const [serviceCode, setServiceCode] = useState('');
  const [serviceName, setServiceName] = useState('');

  const bottomSheetRef = useRef();

  useEffect(() => {
    const fetchJwtToken = async () => {
      const jwt = await getJwtToken();
      setJwtToken(jwt);
    };

    fetchJwtToken();
  }, [jwtToken]);

  const onFocusSelectBank = () => {
    bottomSheetRef.current.open();
  };

  async function onSaveButtonPressed() {
    console.log('addBalanceAccountScreen()::saveButtonPressed');

    let body = {
      service_code: serviceCode,
      service_name: serviceName,
      account_number: number,
      account_name: name,
    };

    axiosHttpRequest(jwtToken)
      .post(URL.BANK_ACCOUNTS, body)
      .then(function (response) {
        console.log('addBalanceAccountScreen()::then', response.data);
        let data = response.data;

        if (data.code === 200) {
          //Success
          alert('Berhasil Menambahkan data');
          navigation.navigate(ScreenName.withdraw);
        } else {
          //Handle Error Response
          alert(`Gagal dengan response code ${data.code}`);
        }
      })
      .catch(function (error) {
        //Handle Error
      });
  }

  return (
    <SafeAreaView>
      <TextInput
        label="Nama"
        value={name}
        mode="outlined"
        style={{marginHorizontal: 16, marginVertical: 8}}
        activeOutlineColor={hexColor.primary}
        onChangeText={text => setName(text)}
      />
      <TextInput
        label="Bank"
        value={serviceName}
        onFocus={onFocusSelectBank}
        mode="outlined"
        style={{marginHorizontal: 16, marginVertical: 8}}
        activeOutlineColor={hexColor.primary}
        onChangeText={text => setBank(text)}
      />
      <TextInput
        label="Nomor Rekening"
        value={number}
        mode="outlined"
        activeOutlineColor={hexColor.primary}
        style={{marginHorizontal: 16, marginVertical: 8}}
        onChangeText={text => setNumber(text)}
      />
      <Button
        onPress={() => onSaveButtonPressed()}
        mode="contained"
        style={{backgroundColor: hexColor.primary, margin: 16}}>
        Simpan
      </Button>

      {jwtToken !== '' ? (
        <BankListBottomSheet
          sheetRef={bottomSheetRef}
          jwtToken={jwtToken}
          setServiceCode={setServiceCode}
          setServiceName={setServiceName}
        />
      ) : (
        <ActivityIndicator animating={true} color={hexColor.primary} />
      )}
    </SafeAreaView>
  );
}
