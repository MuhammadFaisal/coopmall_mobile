import {
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import direction from '../../../style/direction';
import {Button, RadioButton, Switch, Text, TextInput} from 'react-native-paper';
import font_style from '../../../style/font_style';
import {color, hexColor} from '../../../style/color';
import React, {useEffect} from 'react';
import * as Constant from '../../../utils/constant';
import {ProductStatus, ScreenName, String, URL} from '../../../utils/constant';
import {launchImageLibrary} from 'react-native-image-picker';
import getJwtToken from '../../../helper/data_helper';
import {fetch} from 'react-native/Libraries/Network/fetch';
import axios from 'axios';

export default function AddProductScreen({route, navigation}) {
  const {productId} = route.params;

  const [isLoading, setLoading] = React.useState(false);
  const [jwtToken, setJwtToken] = React.useState('');
  const [condition, setCondition] = React.useState('NEW');
  const [name, setName] = React.useState('');
  const [price, setPrice] = React.useState('');
  const [sku, setSku] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [isProductActive, setProductActive] = React.useState(true);
  const [subCategoryId, setSubCategoryId] = React.useState('');
  const [imagesBase64, setImagesBase64] = React.useState('');
  const [images, setImages] = React.useState([
    {
      image: require('../../../images/icon/ic_add.png'),
      isAddButton: true,
    },
  ]);
  const [imagesSelected, setImagesSelected] = React.useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const jwt = await getJwtToken();
      setJwtToken(jwt);
    };

    const fetchDetailProduct = () => {
      let productDetailUrl = Constant.URL.PRODUCT.concat(`/${productId}`);
      console.log('Hit URL ', productDetailUrl);
      axios
        .get(productDetailUrl, {
          headers: {Authorization: `Bearer ${jwtToken}`},
        })
        .then(function (response) {
          let value = response.data.data;
          console.log('addProductScreen::thenFetchDetailProduct()', value);

          let status = value.status;
          setName(value.name);
          setPrice(value.catalogs[0].price.toString());
          setDescription(value.description);
          setSku(value.catalogs[0].sku.toString());

          if (status === ProductStatus.ACTIVE) {
            setProductActive(true);
          } else {
            setProductActive(false);
          }
        })
        .catch(function (response) {
          console.log('addProductScreen::catchFetchDetailProduct()', response);
        });
    };

    fetchData();

    if (productId !== '') {
      console.log('addProductScreen::productId : ', productId);
      navigation.setOptions({
        headerTitle: 'Update Produk',
      });
      fetchDetailProduct();
    }
  }, [jwtToken, productId]);

  function _onProductImagePressed(item) {
    if (item.isAddButton === false) {
      alert('False Button');
    } else {
      let options = {
        quality: 1.0,
        base64: true,
        includeBase64: true,
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
      };

      launchImageLibrary(options, function (response) {
        const assets = response.assets;
        if (assets != null) {
          const asset = assets[0];

          const data = {
            base64: asset.base64,
            uri: asset.uri,
            type: asset.type,
            fileName: asset.fileName,
            isAddButton: false,
          };

          console.log('addProductScreen::launchImageLibraryResponse()', data);

          const currentImage = images.splice(0, 0, data);
          setImagesSelected(currentImage);
        }
      });
    }
  }

  const _imageProductRender = ({item}) => (
    <View style={[margin(4).horizontal, {backgroundColor: 'gray'}]}>
      <TouchableOpacity
        onPress={() => {
          _onProductImagePressed(item);
        }}>
        <Image
          source={item.isAddButton ? item.image : {uri: item.uri}}
          style={[
            item.isAddButton
              ? {width: 24, height: 24, margin: 20}
              : {width: 64, height: 64, resizeMode: 'contain'},
          ]}
        />
      </TouchableOpacity>
    </View>
  );

  const onSwitchChanged = () => setProductActive(!isProductActive);

  return (
    <>
      <ScrollView>
        <SafeAreaView
          style={[direction.column, {backgroundColor: hexColor.white}]}>
          <View
            style={[
              direction.row,
              {justifyContent: 'space-between'},
              padding(16).horizontal,
              margin(24).top,
            ]}>
            <Text style={font_style.normalBold}>Foto Produk</Text>
            <Text style={[font_style.normal, color.primary]}>Tambah Foto</Text>
          </View>
          <View
            style={[direction.row, margin(16).vertical, margin(8).horizontal]}>
            <FlatList
              keyExtractor={(item, index) => index}
              extraData={imagesSelected}
              data={images}
              renderItem={_imageProductRender}
              horizontal={true}
            />
          </View>
          <View
            style={[
              direction.column,
              padding(16).horizontal,
              padding(8).vertical,
            ]}>
            <Text style={font_style.normalBold}>
              Isi Nama Produk Yang Dijual
            </Text>
            <TextInput
              mode="outlined"
              placeholder="Nama Produk"
              dense={true}
              value={name}
              onChangeText={text => setName(text)}
            />
            <Text style={[font_style.normalBold, margin(24).top]}>
              Harga Produk
            </Text>
            <TextInput
              style={margin(8).top}
              mode="flat"
              placeholder="Harga Produk"
              value={price}
              onChangeText={text => setPrice(text)}
              dense={true}
              keyboardType="number-pad"
              left={<TextInput.Affix text="Rp " animated={false} />}
            />
            <Text style={[font_style.normalBold, margin(24).top]}>Stok</Text>
            <TextInput
              style={margin(8).top}
              mode="flat"
              value={sku}
              placeholder="Stok Tersedia"
              dense={true}
              onChangeText={text => setSku(text)}
              keyboardType="number-pad"
            />
            {/*
            <TextInput
              style={margin(8).top}
              mode="flat"
              placeholder="Minimum Pembelian"
              dense={true}
              keyboardType="number-pad"
            />
*/}

            <Text style={[font_style.normalBold, margin(24).top]}>
              Deskripsi Produk
            </Text>
            <TextInput
              style={margin(8).top}
              mode="outlined"
              numberOfLines={8}
              multiline={true}
              value={description}
              placeholder="Tulis Deskripsi Produk"
              onChangeText={text => setDescription(text)}
              dense={true}
            />
            {/*           <View
              style={[
                direction.row,
                {justifyContent: 'space-between'},
                margin(16).top,
              ]}>
              <Text style={font_style.normalBold}>Etalase</Text>
              <Text style={[font_style.normal, color.primary]}>Tambah</Text>
            </View>*/}

            <Text style={[font_style.normalBold, margin(24).top]}>Kondisi</Text>

            <View style={[direction.row, margin(10).top]}>
              <RadioButton
                value="Baru"
                color={hexColor.primary}
                status={condition === 'NEW' ? 'checked' : 'unchecked'}
                onPress={() => setCondition('NEW')}
              />
              <Text
                style={[
                  font_style.normalBold,
                  margin(8).horizontal,
                  {alignSelf: 'center'},
                ]}>
                Baru
              </Text>
            </View>
            <View style={[direction.row, margin(4).top]}>
              <RadioButton
                value="Bekas"
                color={hexColor.primary}
                status={condition === 'PRELOVED' ? 'checked' : 'unchecked'}
                onPress={() => setCondition('PRELOVED')}
              />
              <Text
                style={[
                  font_style.normalBold,
                  margin(8).horizontal,
                  {alignSelf: 'center'},
                ]}>
                Bekas
              </Text>
            </View>
            <View
              style={[
                direction.row,
                {
                  paddingVertical: 10,
                  paddingHorizontal: 16,
                  justifyContent: 'space-between',
                  marginTop: 16,
                },
              ]}>
              <Text style={font_style.normal}>Aktifkan Produk</Text>
              <Switch value={isProductActive} onValueChange={onSwitchChanged} />
            </View>
            <Button
              mode="contained"
              color={hexColor.darkPrimary}
              labelStyle={color.white}
              style={margin(16).top}
              loading={isLoading}
              onPress={() => _onButtonSavePressed()}>
              {String.save}
            </Button>
          </View>
        </SafeAreaView>
      </ScrollView>
    </>
  );

  function _onButtonSavePressed() {
    setLoading(true);
    let productStatus = Constant.ProductStatus.ACTIVE;

    if (!isProductActive) {
      productStatus = Constant.ProductStatus.INACTIVE;
    }

    let file = '';
    for (let i = 0; i < images.length - 1; i++) {
      let asset = images[i];
      if (i !== 0) {
        file += '|';
      }
      file += asset.base64;
    }

    let conditionEn = '';
    if (condition === 'Baru') {
      conditionEn = 'NEW';
    } else {
      conditionEn = 'SECOND';
    }

    let body = '';
    let method = '';

    if (productId === '') {
      method = 'POST';
      body = JSON.stringify({
        name: name,
        files: file,
        price: price,
        sku: sku,
        condition: conditionEn,
        description: description,
        sub_category_id: '1',
        status: productStatus,
      });
    } else {
      method = 'PUT';
      body = JSON.stringify({
        id: productId,
        name: name,
        files: file,
        price: price,
        sku: sku,
        condition: conditionEn,
        description: description,
        sub_category_id: '1',
        status: productStatus,
      });
    }
    console.log('addProductScreen()::body', method, body);
    fetch(Constant.URL.PRODUCT, {
      method: method,
      headers: {
        Authorization: `Bearer ${jwtToken}`,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: body,
    })
      .then(response => response.json())
      .then(json => {
        setLoading(false);
        alert('Berhasil');
        navigation.navigate(ScreenName.listProduct);
        console.log('addProductScreen::then()', json);
      })
      .catch(function (error) {
        console.log('addProductScreen::catch()', error);
        setLoading(false);
        throw error;
      });
  }
}

const margin = (v?: number) =>
  StyleSheet.create({
    all: {
      margin: v,
    },
    start: {
      marginStart: v,
    },
    end: {
      marginEnd: v,
    },
    top: {
      marginTop: v,
    },
    vertical: {
      marginTop: v,
      marginBottom: v,
    },
    horizontal: {
      marginStart: v,
      marginEnd: v,
    },
  });

const padding = (v?: number) =>
  StyleSheet.create({
    all: {
      padding: v,
    },
    start: {
      paddingStart: v,
    },
    end: {
      paddingEnd: v,
    },
    top: {
      paddingTop: v,
    },
    vertical: {
      paddingVertical: v,
    },
    horizontal: {
      paddingHorizontal: v,
    },
  });
