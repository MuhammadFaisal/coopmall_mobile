import {
  SafeAreaView,
  StyleSheet,
  ToastAndroid,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useRef} from 'react';
import {Avatar, Button, TextInput} from 'react-native-paper';
import {ScreenName, String} from '../../../utils/constant';
import {color, hexColor} from '../../../style/color';
import * as Constant from '../../../utils/constant';
import {launchImageLibrary} from 'react-native-image-picker';
import direction from '../../../style/direction';
import styles from '../../../style/styles';
import axios from 'axios';
import ErrorBottomSheet from '../../../template/bottomsheet/error_bottom_sheet';

export default function CreateOutletScreen({navigation}) {
  const [textOutletName, setOutletName] = React.useState('');
  const [textDomain, setDomain] = React.useState('');
  const [textDescription, setDescription] = React.useState('');
  const [textAddress, setAddress] = React.useState('');
  const [imageOutlet, setImageOutlet] = React.useState('');
  const [imageOutletBase64, setImageOutletBase64] = React.useState('');
  const [isCheckingDomain, setCheckingDomain] = React.useState(false);
  const [isLoading, setLoading] = React.useState(false);
  const [isDomainValid, setDomainValid] = React.useState(false);

  const [errTitle, setErrTitle] = React.useState('');
  const [errDesc, setErrDesc] = React.useState('');

  const bottomSheetRef = useRef();

  function _onButtonCreatePressed() {
    if (isDomainValid) {
      setLoading(true);
      let json = JSON.stringify({
        name: textOutletName,
        domain: textDomain,
        description: textDescription,
        Address: textAddress,
        file: imageOutletBase64,
      });

      console.log('createOutletScreen()::onButtonCreatePressed', json);
      fetch(Constant.URL.STORE, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: json,
      })
        .then(response => response.json())
        .then(json => {
          console.log(json);
          setLoading(false);
          navigation.push(ScreenName.myOutlet);
        })
        .catch(function (error) {
          console.log(error);
          setLoading(false);
          throw error;
        });
    } else {
      setErrTitle('Domain Tidak Valid');
      setErrDesc(
        'Pastikan anda telah cek ketersediaan domain anda sebelum melanjutkan!',
      );
      bottomSheetRef.current.open();
    }
  }

  function onOutletImagePressed() {
    let options = {
      quality: 1.0,
      base64: true,
      includeBase64: true,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    launchImageLibrary(options, function (response) {
      const assets = response.assets;

      if (assets != null) {
        const asset = assets[0];

        const data = {
          base64: asset.base64,
          uri: asset.uri,
          type: asset.type,
          fileName: asset.fileName,
          isAddButton: false,
        };

        console.log('addProductScreen::launchImageLibraryResponse()', data);
        setImageOutletBase64(asset.base64);
        setImageOutlet(asset.uri);
      }
    });
  }

  function onButtonCheckDomainPressed() {
    setCheckingDomain(true);

    let url = `${Constant.URL.ROOT}/my/stores/domain/check-domain/${textDomain}`;

    console.log('createOutletScreen()::buttonCheckPressed', url);
    axios
      .get(url)
      .then(response => {
        setCheckingDomain(false);
        let data = response.data;
        console.log('createOutletScreen()::then', data);

        if (data.code == 200) {
          setDomainValid(true);
          ToastAndroid.show('Domain Dapat Digunakan', ToastAndroid.SHORT);
        } else {
          setErrTitle('Domain Telah Digunakan');
          setErrDesc(
            'Coba untuk menggunakan domain unik lainnya yang tersedia',
          );
          bottomSheetRef.current.open();
        }
      })
      .catch(err => {
        setCheckingDomain(false);
        console.log('createOutletScreen()::catch', err);
      });
  }

  return (
    <>
      <SafeAreaView style={{flex: 1}}>
        <View style={{alignItems: 'center', margin: 24}}>
          <TouchableOpacity onPress={() => onOutletImagePressed()}>
            <Avatar.Image
              source={
                !imageOutlet
                  ? require('../../../images/icon/ic_add.png')
                  : {uri: imageOutlet}
              }
              size={128}
              style={{
                backgroundColor: hexColor.gray,
              }}
            />
          </TouchableOpacity>
        </View>
        <TextInput
          style={[margin(16).horizontal, margin(8).vertical]}
          mode="outlined"
          defaultValue={textOutletName}
          dense={true}
          placeholder={String.outletName}
          activeOutlineColor={hexColor.primary}
        />
        <View
          style={[
            direction.row,
            margin(16).horizontal,
            margin(8).vertical,
            {alignItems: 'center'},
          ]}>
          <TextInput
            style={{flex: 1}}
            mode="outlined"
            dense={true}
            defaultValue={textDomain}
            onChangeText={text => setDomain(text)}
            left={<TextInput.Affix text="www.coopmall.com/" animated={false} />}
            placeholder={String.domainName}
            activeOutlineColor={hexColor.primary}
          />
          <Button
            style={[styles.buttonPrimary, {flex: 0.2, margin: 4}]}
            color={hexColor.white}
            loading={isCheckingDomain}
            onPress={() => onButtonCheckDomainPressed()}>
            Cek Domain
          </Button>
        </View>
        <TextInput
          style={[margin(16).horizontal, margin(8).vertical]}
          mode="outlined"
          dense={true}
          numberOfLines={4}
          activeOutlineColor={hexColor.primary}
          defaultValue={textDescription}
          placeholder={String.description}
        />
        <TextInput
          style={[margin(16).horizontal, margin(8).vertical]}
          mode="outlined"
          numberOfLines={4}
          dense={true}
          activeOutlineColor={hexColor.primary}
          defaultValue={textAddress}
          placeholder={String.address}
        />

        <Button
          style={[margin(16).horizontal, margin(8).vertical]}
          color={hexColor.darkPrimary}
          mode="contained"
          loading={isLoading}
          onPress={() => _onButtonCreatePressed()}>
          {String.createOutlet}
        </Button>

        <ErrorBottomSheet
          sheetRef={bottomSheetRef}
          title={errTitle}
          description={errDesc}
          isUseHelpButton={false}
        />
      </SafeAreaView>
    </>
  );
}

const margin = (v?: number) =>
  StyleSheet.create({
    all: {
      margin: v,
    },
    start: {
      marginStart: v,
    },
    end: {
      marginEnd: v,
    },
    vertical: {
      marginTop: v,
      marginBottom: v,
    },
    horizontal: {
      marginStart: v,
      marginEnd: v,
    },
  });
