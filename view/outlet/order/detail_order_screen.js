import {FlatList, Image, ScrollView, View} from 'react-native';
import direction from '../../../style/direction';
import font_style from '../../../style/font_style';
import {Button, Divider, Snackbar, Text} from 'react-native-paper';
import React, {useEffect} from 'react';
import {color, hexColor} from '../../../style/color';
import axios from 'axios';
import {ScreenName, URL} from '../../../utils/constant';
import {
  currencyFormat,
  featuredImageText,
} from '../../../helper/general_helper';

export default function DetailOrderScreen({route, navigation}) {
  const {transactionId} = route.params;
  const {jwtToken} = route.params;

  const [isButtonLoading, setButtonLoading] = React.useState(false);
  const [data, setData] = React.useState({});
  const [delivery, setDelivery] = React.useState({});
  const [payment, setPayment] = React.useState({});
  const [detail, setDetail] = React.useState([]);
  const [isVisible, setVisible] = React.useState(false);

  useEffect(() => {
    console.log(
      'detailOrderScreen()::useEffect',
      `Inside useEffect with trxId ${transactionId}`,
    );

    const getData = async () => {
      let url = URL.LIST_ORDER.concat(`/${transactionId}`).concat('/detail');
      console.log(url, jwtToken);
      await axios
        .get(url, {headers: {Authorization: `Bearer ${jwtToken}`}})
        .then(function (response) {
          setData(response.data.data);
          setDetail(response.data.data.detail);
          setDelivery(response.data.data.info_delivery);
          setPayment(response.data.data.summary_payment);
          console.log('detailOrderScreen()::then', detail);
        })
        .catch(function (error) {
          console.log('detailOrderScreen()::error', error);
        });
    };

    getData();
  }, []);

  const OrderInfo = () => (
    <View
      style={[direction.column, {padding: 8, backgroundColor: hexColor.white}]}>
      <Text style={[font_style.miniHeaderBold, {padding: 8}]}>
        Pesanan Baru
      </Text>
      <Divider />
      <Text style={[font_style.caption, {padding: 8}]}>{data.invoice_no}</Text>
      <Text
        style={[
          font_style.caption,
          {paddingHorizontal: 8, paddingTop: 8},
          color.gray,
        ]}>
        Nama Pembeli
      </Text>
      <Text style={[font_style.normal, {paddingHorizontal: 8}]}>
        {data.name}
      </Text>
      <Text
        style={[
          font_style.caption,
          {paddingHorizontal: 8, paddingTop: 8},
          color.gray,
        ]}>
        Tanggal Transaksi
      </Text>
      <Text style={[font_style.normal, {paddingHorizontal: 8}]}>
        {data.trxDate}
      </Text>
    </View>
  );

  const DetailProduct = ({item}) => {
    console.log('detailProduct()', item);
    const detail = item[0];

    if (item.length > 0) {
      return (
        <View
          style={[
            direction.column,
            {marginTop: 8, backgroundColor: hexColor.white, padding: 16},
          ]}>
          <Text style={font_style.miniHeaderBold}>Detail Produk</Text>
          <FlatList data={item} renderItem={productRenderItem} />
        </View>
      );
    }

    return <View />;
  };

  const ExpeditionInfo = () => (
    <View
      style={[
        direction.column,
        {marginTop: 8, backgroundColor: hexColor.white, padding: 16},
      ]}>
      <Text style={font_style.miniHeaderBold}>Info Pengiriman</Text>
      <Divider style={{marginTop: 8}} />
      <View style={[direction.row, {marginTop: 8}]}>
        <Text style={{width: '50%', color: hexColor.gray}}>
          Jenis Pengiriman
        </Text>
        <Text style={{width: '50%'}}>-</Text>
      </View>
      <View style={[direction.row, {marginTop: 8}]}>
        <Text style={{width: '50%', color: hexColor.gray}}>No Resi</Text>
        <Text style={{width: '50%'}}>-</Text>
      </View>
      <View style={[direction.row, {marginTop: 8}]}>
        <Text style={{width: '50%', color: hexColor.gray}}>Alamat</Text>
        <Text style={{width: '50%'}}>{delivery.address}</Text>
      </View>
    </View>
  );

  const PaymentSummary = ({item}) => {
    console.log('paymentSummary()', item);
    return (
      <View
        style={[
          direction.column,
          {marginTop: 8, backgroundColor: hexColor.white, padding: 16},
        ]}>
        <Text style={font_style.miniHeaderBold}>Ringkasan Pembayaran</Text>
        <Divider style={{marginTop: 8}} />
        <View style={[direction.row, {marginTop: 8}]}>
          <Text style={{width: '50%'}}>Total Harga</Text>
          <Text style={{width: '50%', textAlign: 'right'}}>
            {item.total_price != null
              ? currencyFormat(item.total_price)
              : currencyFormat(0)}
          </Text>
        </View>
        <View style={[direction.row, {marginTop: 8}]}>
          <Text style={{width: '50%'}}>Total Ongkos Kirim</Text>
          <Text style={{width: '50%', textAlign: 'right'}}>
            {item.total_shipping_cost != null
              ? currencyFormat(item.total_shipping_cost)
              : currencyFormat(0)}
          </Text>
        </View>
        <View style={[direction.row, {marginTop: 8}]}>
          <Text style={{width: '50%'}}>Biaya Admin</Text>
          <Text style={{width: '50%', textAlign: 'right'}}>
            {item.shipping_cost_must_be_paid != null
              ? currencyFormat(item.shipping_cost_must_be_paid)
              : currencyFormat(0)}
          </Text>
        </View>
        <View style={[direction.row, {marginTop: 8}]}>
          <Text
            style={[
              font_style.normalBold,
              {width: '50%', color: hexColor.black},
            ]}>
            Total Penjualan
          </Text>
          <Text style={[font_style.normal, {width: '50%', textAlign: 'right'}]}>
            {item.total_sales != null
              ? currencyFormat(item.total_sales)
              : currencyFormat(0)}
          </Text>
        </View>
      </View>
    );
  };

  function productRenderItem({item, index}) {
    let featured_image = item.featured_image;
    let endpoint = '';
    if (featured_image != null) {
      endpoint = featured_image.formats.thumbnail.url;
    }
    return (
      <View
        style={[
          direction.row,
          {
            marginTop: 8,
            padding: 8,
            borderStyle: 'solid',
            borderWidth: 1,
            borderColor: hexColor.darkGray,
            borderRadius: 16,
          },
        ]}>
        {endpoint !== '' ? (
          <Image
            source={{uri: `http://dev.coopaccess.co.id:1338${endpoint}`}}
            style={{width: 56, height: 56, resizeMode: 'contain'}}
          />
        ) : (
          <View
            style={{
              width: 56,
              height: 56,
              backgroundColor: hexColor.primary,
              borderRadius: 8,
              borderWidth: 1,
              borderColor: hexColor.primary,
              justifyContent: 'center',
            }}>
            <Text
              style={[
                font_style.headerBold,
                {alignSelf: 'center'},
                color.white,
              ]}>
              {item.name !== undefined ? featuredImageText(item.name) : '?'}
            </Text>
          </View>
        )}

        <Text style={[font_style.normalBold, {alignSelf: 'center', margin: 8}]}>
          {item.product_name}
        </Text>
      </View>
    );
  }

  function onButtonPressed() {
    setButtonLoading(true);
    const headers = {
      headers: {Authorization: `Bearer ${jwtToken}`},
    };

    axios
      .patch(
        URL.LIST_ORDER,
        {
          id: transactionId,
          delivery_status: 'ORDER_PROCESSING',
        },
        headers,
      )
      .then(function (response) {
        navigation.navigate(ScreenName.sellingList, {status: 'Pesanan Baru'});
      })
      .catch(function (error) {
        alert(error);
        setButtonLoading(false);
      });
  }

  const onDismissSnackbar = () => {
    setButtonLoading(false);
    setVisible(false);
  };

  return (
    <>
      <ScrollView>
        <View style={[direction.column, {backgroundColor: hexColor.softGray}]}>
          <OrderInfo />
          <DetailProduct item={detail} />
          <ExpeditionInfo />
          <PaymentSummary item={payment} />
          <Button
            mode="contained"
            loading={isButtonLoading}
            onPress={() => onButtonPressed()}
            style={{
              backgroundColor: hexColor.primary,
              marginVertical: 16,
              marginHorizontal: 8,
            }}>
            <Text style={{color: hexColor.white}}>Proses Pesanan</Text>
          </Button>
        </View>
      </ScrollView>
    </>
  );
}
