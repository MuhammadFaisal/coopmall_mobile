import {
  FlatList,
  Image,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import {useEffect} from 'react';
import getJwtToken from '../../../helper/data_helper';
import axios from 'axios';
import {ScreenName, URL} from '../../../utils/constant';
import React from 'react';
import {Button, Text, TextInput} from 'react-native-paper';
import {color, hexColor} from '../../../style/color';
import direction from '../../../style/direction';
import font_style from '../../../style/font_style';

export default function ListProductScreen({navigation}) {
  const [jwtToken, setJwtToken] = React.useState('');
  const [products, setProducts] = React.useState([]);

  useEffect(() => {
    const fetchJwtToken = async () => {
      const jwt = await getJwtToken();
      setJwtToken(jwt);
    };

    const getProductList = () => {
      axios
        .get(URL.LIST_PRODUCT_SELLER, {
          headers: {Authorization: `Bearer ${jwtToken}`},
        })
        .then(function (response) {
          setProducts(response.data.data);
          console.log('listProductScreen::then() ', response.data.data);
        })
        .catch(function (response) {
          console.log('listProductScreen::catch()', response);
        });
    };

    fetchJwtToken();
    getProductList();
  }, [jwtToken]);

  function _onProductPressed(item) {
    console.log(
      'listProductScreen::onPressed()',
      `Push Product Id ${item.id} to Detail Product Screen.`,
    );
    navigation.push(ScreenName.addProduct, {
      productId: item.id,
    });
  }

  function _onButtonChangePricePressed() {
    alert('ok');
  }

  function _onButtonMorePressed(item) {
    let deleteUrl = URL.PRODUCT + `/${item.id}`;
    console.log(
      'listProductScreen()::onMoreButtonPressed',
      `Start: Deleting Item ${item.id}`,
      `Hit URL ${deleteUrl}`,
    );

    axios
      .delete(deleteUrl, {
        headers: {
          Authorization: `Bearer ${jwtToken}`,
        },
      })
      .then(result => {
        console.log('listProductScreen()::result', result.data);
        alert('Item Berhasil Dihapus!');
        navigation.navigate(ScreenName.myOutlet);
      })
      .catch(error => {
        alert(error);
      });
  }

  const productRender = ({item}) => {
    const endpoint = item.featured_image.formats.thumbnail.url;
    return (
      <TouchableOpacity onPress={() => _onProductPressed(item)}>
        <View
          style={[
            direction.column,
            padding(8).all,
            {backgroundColor: 'white'},
          ]}>
          <View style={direction.row}>
            <Image
              source={{uri: `http://dev.coopaccess.co.id:1338${endpoint}`}}
              style={{width: 56, height: 56, resizeMode: 'contain'}}
            />
            <View style={[direction.column, margin(16).horizontal]}>
              <Text style={[font_style.normalBold, margin(8).end]}>
                {item.name}
              </Text>
              <Text style={font_style.normal}>Rp{item.catalogs[0].price}</Text>
              <Text style={[font_style.caption, margin(8).top, color.gray]}>
                Stok {item.catalogs[0].sku}
              </Text>
            </View>
          </View>

          <View
            style={[direction.row, margin(8).top, {justifyContent: 'center'}]}>
            <Button
              mode="outlined"
              color={hexColor.black}
              style={[margin(4).horizontal, {width: '45%'}]}>
              Ubah Harga
            </Button>
            <Button
              mode="outlined"
              onPress={() => _onButtonChangePricePressed()}
              color={hexColor.black}
              style={[margin(4).horizontal, {width: '45%'}]}>
              Ubah Stok
            </Button>
            <TouchableOpacity onPress={() => _onButtonMorePressed(item)}>
              <Image
                style={[
                  padding(8).all,
                  {alignSelf: 'center', resizeMode: 'contain'},
                ]}
                source={require('../../../images/icon/ic_more_vertical.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <>
      <SafeAreaView style={{flex: 1, backgroundColor: '#F2F2F2'}}>
        <View style={[direction.column, {backgroundColor: 'white'}]}>
          <TextInput
            style={margin(16).all}
            placeholder="Cari Produk"
            mode="outlined"
            dense={true}
            left={<TextInput.Icon name={'magnify'} animated={false} />}
          />
          <View
            style={[
              direction.row,
              margin(16).horizontal,
              margin(8).vertical,
              {justifyContent: 'space-between'},
            ]}>
            <Text style={[font_style.normal, color.gray]}>1 Produk</Text>
            <Text style={[font_style.normalBold, color.primary]}>Pilih</Text>
          </View>
        </View>
        <FlatList
          style={margin(4).top}
          data={products}
          renderItem={productRender}
          keyExtractor={item => item.id}
        />
      </SafeAreaView>
    </>
  );
}

const margin = (v?: number) =>
  StyleSheet.create({
    all: {
      margin: v,
    },
    start: {
      marginStart: v,
    },
    end: {
      marginEnd: v,
    },
    top: {
      marginTop: v,
    },
    vertical: {
      marginTop: v,
      marginBottom: v,
    },
    horizontal: {
      marginStart: v,
      marginEnd: v,
    },
  });

const padding = (v?: number) =>
  StyleSheet.create({
    all: {
      padding: v,
    },
    start: {
      paddingStart: v,
    },
    end: {
      paddingEnd: v,
    },
    top: {
      paddingTop: v,
    },
    vertical: {
      paddingVertical: v,
    },
    horizontal: {
      paddingHorizontal: v,
    },
  });
