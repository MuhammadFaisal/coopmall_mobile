import {
  ActivityIndicator,
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import direction from '../../../style/direction';
import {Chip, Divider, TextInput} from 'react-native-paper';
import React, {useEffect, useState} from 'react';
import {color, hexColor} from '../../../style/color';
import font_style from '../../../style/font_style';
import {ScreenName} from '../../../utils/constant';
import axios from 'axios';
import {URL} from '../../../utils/constant';
import getJwtToken from '../../../helper/data_helper';
import {stat} from '@babel/core/lib/gensync-utils/fs';
import {featuredImageText} from '../../../helper/general_helper';

export default function SellingListScreen({route, navigation}) {
  const [items, setItems] = React.useState([]);
  const [suffix, setSuffix] = React.useState('');
  const [jwtToken, setJwtToken] = React.useState('');
  const [products, setProducts] = React.useState([]);
  const [selectedId, setSelectedId] = React.useState(null);
  const {status} = route.params;
  const [orderStatus, setOrderStatus] = React.useState(status);
  const [isLoading, setLoading] = useState(false);

  let options = [
    {
      id: 1,
      name: 'Pesanan Baru',
      endpoint: '/ORDER_RECEIVED',
    },
    {
      id: 2,
      name: 'Pesanan Diproses',
      endpoint: '/ORDER_PROCESSING',
    },
    {
      id: 3,
      name: 'Pesenan Selesai',
      endpoint: '/ORDER_DELIVERED',
    },
    {
      id: 3,
      name: 'Pesenan Dibatalkan',
      endpoint: '/ORDER_CANCEL',
    },
  ];

  function onChipPressed(item) {
    setOrderStatus(item.name);
    getItemList(item.name);
  }

  const getItemList = statusOrder => {
    setLoading(true);
    let endpoint = status;
    if (statusOrder === 'Pesanan Baru') {
      endpoint = 'ORDER_RECEIVED';
    } else if (statusOrder === 'Pesanan Diproses') {
      endpoint = 'ORDER_PROCESSING';
    } else if (statusOrder === 'Pesanan Selesai') {
      endpoint = 'ORDER_DELIVERED';
    } else {
      endpoint = 'ORDER_CANCEL';
    }

    let url = URL.LIST_ORDER + `/${endpoint}`;
    console.log('sellingListScreen()::getItemList', 'started();', url);
    axios
      .get(url, {headers: {Authorization: `Bearer ${jwtToken}`}})
      .then(response => {
        setItems(response.data.data);

        for (let i = 0; i < items.length; i++) {
          let transactionDetails = items[i].detail;
          setProducts(transactionDetails);
        }

        setLoading(false);
        console.log('sellingListScreen()::then', items, products);
      })
      .catch(error => {
        console.log('sellingListScreen()::error', error);
      });

    console.log('sellingListScreen()::getItemList', 'ended();');
  };

  useEffect(() => {
    const fetchJwtToken = async () => {
      const jwt = await getJwtToken();
      setJwtToken(jwt);
    };

    fetchJwtToken();
    getItemList(status);
  }, [jwtToken]);

  const _optionRenderItem = ({item, index}) => {
    const isSelected = item.name === orderStatus;
    const selectedColor =
      isSelected === true ? hexColor.primary : hexColor.gray;
    const emptyIcon = () => null;

    return (
      <Chip
        onPress={() => onChipPressed(item)}
        selected={isSelected}
        selectedColor={selectedColor}
        icon={emptyIcon}
        style={{margin: 4, backgroundColor: hexColor.white}}
        mode="outlined">
        {item.name}
      </Chip>
    );
  };

  function _onItemPressed(item) {
    navigation.navigate(ScreenName.detailOrder, {
      transactionId: item.id,
      jwtToken: jwtToken,
    });
  }

  const productItemRender = ({item}) => {
    let featured_image = item.featured_image;
    let endpoint = '';
    if (featured_image != null) {
      endpoint = featured_image.formats.thumbnail.url;
    }
    return (
      <View style={[direction.row, {marginVertical: 8, marginHorizontal: 16}]}>
        {endpoint !== '' ? (
          <Image
            source={{uri: `http://dev.coopaccess.co.id:1338${endpoint}`}}
            style={{width: 56, height: 56, resizeMode: 'contain'}}
          />
        ) : (
          <View
            style={{
              width: 56,
              height: 56,
              backgroundColor: hexColor.primary,
              borderRadius: 8,
              borderWidth: 1,
              borderColor: hexColor.primary,
              justifyContent: 'center',
            }}>
            <Text
              style={[
                font_style.headerBold,
                {alignSelf: 'center'},
                color.white,
              ]}>
              {item.name !== undefined ? featuredImageText(item.name) : '?'}
            </Text>
          </View>
        )}
        <Text
          style={[
            font_style.miniHeaderBold,
            color.black,
            {alignSelf: 'center', marginLeft: 16, width: '80%'},
          ]}>
          {item.product_name}
        </Text>
      </View>
    );
  };

  const _itemsRenderItem = ({item, index}) => {
    console.log(item, index);
    return (
      <TouchableOpacity onPress={() => _onItemPressed(item)}>
        <View
          style={[
            direction.column,
            {
              margin: 8,
              borderRadius: 8,
              borderWidth: 1,
              borderColor: hexColor.white,
              backgroundColor: hexColor.white,
            },
          ]}>
          <View style={direction.row}>
            <View
              style={{
                backgroundColor: hexColor.darkGray,
                width: 4,
                marginVertical: 8,
              }}
            />
            <View style={[direction.column, {padding: 8}]}>
              <Text style={[font_style.normalBold, color.black]}>
                {item.delivery_status}
              </Text>
              <Text style={font_style.normal}>{item.invoice_no}</Text>
              <Text style={font_style.normal}>{item.name}</Text>
            </View>
          </View>
          <View
            style={{backgroundColor: hexColor.darkGray, height: 0.4, margin: 8}}
          />
          <FlatList
            data={item.detail}
            renderItem={productItemRender}
            keyExtractor={item => item.id}
          />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <>
      <SafeAreaView>
        <View style={direction.column}>
          {/*            <TextInput
              mode="outlined"
              placeholder="Cari produk atau invoice"
              dense={true}
              style={{margin: 8}}
            />*/}

          <FlatList
            style={{padding: 8}}
            data={options}
            renderItem={_optionRenderItem}
            horizontal={true}
          />
          {isLoading ? (
            <ActivityIndicator animating={true} color={hexColor.primary} />
          ) : (
            <FlatList
              style={{padding: 8}}
              data={items}
              renderItem={_itemsRenderItem}
            />
          )}
        </View>
      </SafeAreaView>
    </>
  );
}
