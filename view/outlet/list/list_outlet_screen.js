import {FlatList, SafeAreaView, View} from 'react-native';
import axios from 'axios';
import {ScreenName, URL} from '../../../utils/constant';
import {ActivityIndicator, Text, TouchableRipple} from 'react-native-paper';
import {color, hexColor} from '../../../style/color';
import React, {useEffect} from 'react';
import direction from '../../../style/direction';
import font_style from '../../../style/font_style';
import {featuredImageText} from '../../../helper/general_helper';

export default function ListOutletScreen({route, navigation}) {
  const [isLoading, setLoading] = React.useState(true);
  const [outlets, setOutlets] = React.useState([]);

  const {jwtToken} = route.params;

  useEffect(() => {
    const getBusinesses = () => {
      let jwt = JSON.stringify(jwtToken).replace(/["']/g, '');

      axios
        .get(URL.LIST_OUTLET, {
          headers: {Authorization: `Bearer ${jwt}`},
        })
        .then(function (response) {
          setOutlets(response.data.data);
          setLoading(false);
          console.log('listOutletScreen : ', response.data.data);
        })
        .catch(function (response) {
          console.log('listOutletScreen : ', response);
          setLoading(false);
        });
    };

    getBusinesses();
  }, [jwtToken, route.params]);

  function _onOutletClicked(id) {
    navigation.push(ScreenName.myOutlet, {outletId: id});
  }

  const _outletRenderItem = ({item}) => (
    <TouchableRipple
      rippleColor={hexColor.primary}
      onPress={() => _onOutletClicked(item.id)}>
      <View style={[direction.row, {padding: 16}]}>
        <View
          style={{
            width: 64,
            height: 64,
            backgroundColor: hexColor.primary,
            borderRadius: 8,
            borderWidth: 1,
            borderColor: hexColor.primary,
            justifyContent: 'center',
          }}>
          <Text
            style={[font_style.headerBold, {alignSelf: 'center'}, color.white]}>
            {featuredImageText(item.name)}
          </Text>
        </View>
        <View
          style={[direction.column, {padding: 8, justifyContent: 'center'}]}>
          <Text style={font_style.headerBold}>{item.name}</Text>
          <Text style={[font_style.normal, {marginTop: 8}]}>
            {item.description}
          </Text>
        </View>
      </View>
    </TouchableRipple>
  );

  return (
    <>
      <SafeAreaView>
        {isLoading && (
          <ActivityIndicator
            animating={true}
            color={hexColor.primary}
            style={{margin: 24}}
          />
        )}
        <FlatList
          data={outlets}
          renderItem={_outletRenderItem}
          keyExtractor={outlet => outlet.id}
        />
      </SafeAreaView>
    </>
  );
}
