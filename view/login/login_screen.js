import {
    SafeAreaView,
    StatusBar,
    Text,
    View,
    StyleSheet,
    TouchableOpacity, Image,
} from 'react-native';

import React, {useEffect, useRef} from 'react';
import {Button, TextInput as TextInputPaper} from 'react-native-paper';
import {URL, String, Key, ScreenName} from '../../utils/constant';
import {color, hexColor} from '../../style/color';
import * as Constant from '../../utils/constant';
import AsyncStorage from '@react-native-async-storage/async-storage';
import direction from '../../style/direction';
import getJwtToken from '../../helper/data_helper';
import font_style from '../../style/font_style';
import RBSheet from 'react-native-raw-bottom-sheet';
import ErrorBottomSheet from '../../template/bottomsheet/error_bottom_sheet';

export default function LoginScreen({navigation}) {
  const [textIdentifier, setIdentifier] = React.useState('');
  const [textPassword, setPassword] = React.useState('');
  const [isButtonLoading, setButtonLoading] = React.useState(false);
  const [jwtToken, setJwtToken] = React.useState('');

  const refBottomSheet = useRef();

  const storeData = async (key, value) => {
    try {
      await AsyncStorage.setItem(key, value);
    } catch (e) {
      throw e;
    }
  };

  useEffect(() => {
    const checkSession = async () => {
      let currentToken = await getJwtToken();

      console.log('JWT Token is not Empty? ' + [currentToken !== null]);
      if (currentToken !== '') {
        navigation.push(ScreenName.myOutlet);
      }
    };

    checkSession();
  }, [navigation]);

  function _onButtonLoginPressed() {
    setButtonLoading(true);
    fetch(Constant.URL.LOGIN, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: textIdentifier,
        password: textPassword,
      }),
    })
      .then(response => response.json())
      .then(json => {
        let jwtToken = json.jwt;
        storeData(Key.jwtToken, jwtToken).then(result =>
          navigation.push(ScreenName.myOutlet, {jwtToken: jwtToken}),
        );
      })
      .catch(function (error) {
        console.log(error);
        setButtonLoading(false);
        throw error;
      });
  }

  function _onTextRegisterPressed() {
    navigation.push(ScreenName.register);
  }

  function bottomSheet() {
    refBottomSheet.current.open();
  }

  return (
    <>
      <SafeAreaView style={{flex: 1}}>
        <Image source={require('../../images/coop_mall_logo.png')} style={{width: 256, height: 256, resizeMode: 'contain', alignSelf: 'center'}} />
        <TextInputPaper
          style={[margin(16).horizontal, margin(8).vertical]}
          label={String.emailOrPhone}
          mode="outlined"
          activeOutlineColor={hexColor.primary}
          defaultValue={textIdentifier}
          dense={true}
          onChangeText={text => setIdentifier(text)}
        />

        <TextInputPaper
          style={[margin(16).horizontal, margin(8).vertical]}
          label={String.password}
          mode="outlined"
          activeOutlineColor={hexColor.primary}
          defaultValue={textPassword}
          dense={true}
          secureTextEntry={true}
          onChangeText={text => setPassword(text)}
        />

        <Button
          style={[margin(16).horizontal, margin(8).vertical]}
          color={hexColor.darkPrimary}
          mode="contained"
          loading={isButtonLoading}
          onPress={() => _onButtonLoginPressed()}>
          {String.login}
        </Button>

        <View
          style={[
            direction.row,
            {justifyContent: 'space-between'},
            margin(16).horizontal,
            margin(8).vertical,
          ]}>
            <Text style={[color.primary, font_style.normal]}>
                Belum Punya Akun?
            </Text>
          <TouchableOpacity onPress={() => _onTextRegisterPressed()}>
            <Text style={[color.primary, font_style.normal]}>
              Daftar disini
            </Text>
          </TouchableOpacity>
          <ErrorBottomSheet
            sheetRef={refBottomSheet}
            title="Terjadi Kesalahan 2"
          />
        </View>
      </SafeAreaView>
    </>
  );
}

const padding = (v?: number) =>
  StyleSheet.create({
    start: {
      paddingStart: v,
    },
    end: {
      paddingEnd: v,
    },
    top: {
      paddingTop: v,
    },

    bottom: {
      paddingBottom: v,
    },

    horizontal: {
      paddingBottom: v,
    },

    vertical: {
      paddingVertical: v,
    },

    all: {
      padding: v,
    },
  });

const margin = (v?: number) =>
  StyleSheet.create({
    all: {
      margin: v,
    },
    start: {
      marginStart: v,
    },
    end: {
      marginEnd: v,
    },
    vertical: {
      marginTop: v,
      marginBottom: v,
    },
    horizontal: {
      marginStart: v,
      marginEnd: v,
    },
  });
