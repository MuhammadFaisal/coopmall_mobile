import {FlatList, SafeAreaView, View} from 'react-native';
import direction from '../../style/direction';
import React, {useEffect} from 'react';
import {
  ActivityIndicator,
  Divider,
  Text,
  TouchableRipple,
} from 'react-native-paper';
import {color, hexColor} from '../../style/color';
import font_style from '../../style/font_style';
import axios from 'axios';
import {ScreenName, URL} from '../../utils/constant';
import {featuredImageText} from '../../helper/general_helper';

export default function ListBusinessScreen({route, navigation}) {
  const [businesses, setBusinesses] = React.useState([]);
  const {jwtToken} = route.params;
  const [isLoading, setLoading] = React.useState(true);

  useEffect(() => {
    const getBusinesses = () => {
      let jwt = JSON.stringify(jwtToken).replace(/["']/g, '');

      axios
        .get(URL.LIST_BUSINESS, {
          headers: {Authorization: `Bearer ${jwt}`},
        })
        .then(function (response) {
          setBusinesses(response.data.data);
          setLoading(false);
          console.log('listBusinessScreen : ', response.data.data);
        })
        .catch(function (response) {
          console.log('listBusinessScreen : ', response);
          setLoading(false);
        });
    };

    getBusinesses();
  }, [jwtToken, route.params]);

  function _textFeaturedImage(name) {
    let splitName = name.split(' ');
    let value = '';

    if (splitName.length > 1) {
      for (let i = 0; i < splitName.length; i++) {
        let data = splitName[i].charAt(0);

        if (i <= 2) {
          value += data;
        }
      }
    } else {
      value = splitName[0].charAt(0);
    }

    return value;
  }

  function _onBusinessClicked(id) {
    return navigation.push(ScreenName.listOutlet, {
      businessId: id,
      jwtToken: jwtToken,
    });
  }

  const businessRenderItem = ({item}) => (
    <TouchableRipple
      rippleColor={hexColor.primary}
      onPress={() => _onBusinessClicked(item.id)}>
      <View style={[direction.row, {padding: 16}]}>
        <View
          style={{
            width: 64,
            height: 64,
            backgroundColor: hexColor.primary,
            borderRadius: 8,
            borderWidth: 1,
            borderColor: hexColor.primary,
            justifyContent: 'center',
          }}>
          <Text
            style={[font_style.headerBold, {alignSelf: 'center'}, color.white]}>
            {featuredImageText(item.name)}
          </Text>
        </View>
        <View
          style={[direction.column, {padding: 8, justifyContent: 'center'}]}>
          <Text style={font_style.headerBold}>{item.name}</Text>
          <Text style={[font_style.normal, {marginTop: 8}]}>
            {item.description}
          </Text>
        </View>
      </View>
    </TouchableRipple>
  );

  return (
    <>
      <SafeAreaView>
        <View style={direction.column}>
          {console.log('isLoading = ' + isLoading)}
          {isLoading && (
            <ActivityIndicator animating={true} color={hexColor.primary} />
          )}
          <FlatList
            data={businesses}
            renderItem={businessRenderItem}
            keyExtractor={business => business.id}
            ItemSeparatorComponent={() => <Divider />}
          />
        </View>
      </SafeAreaView>
    </>
  );
}
