import {SafeAreaView, View} from 'react-native';
import {Avatar, Text} from 'react-native-paper';
import React from 'react';
import {Key} from '../../utils/constant';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function ProfileScreen({navigation}) {
  const [jwtToken, setJwtToken] = React.useState('');
  const getData = async () => {
    const value = await AsyncStorage.getItem(Key.jwtToken);
    if (value != null) {
      return value;
    }

    return '';
  };

  function _getJwtToken(): String {
    getData().then(r => setJwtToken(r));
  }

  return (
    <>
      <SafeAreaView style={{backgroundColor: 'white', flex: 1}}>

      </SafeAreaView>
    </>
  );
}
