import {
  Avatar,
  Button,
  Text,
  TextInput,
  TouchableRipple,
} from 'react-native-paper';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useRef} from 'react';
import * as Constant from '../../utils/constant';
import {ScreenName, String, URL} from '../../utils/constant';
import FontStyle from '../../style/font_style';
import direction from '../../style/direction';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CoopListBottomSheet from '../../template/bottomsheet/coop_list_bottom_sheet';
import axios from 'axios';

export default function RegisterFormScreen({navigation}) {
  const [isLoading, setLoading] = React.useState(false);

  const [email, setEmail] = React.useState('');
  const [phone, setPhone] = React.useState('');
  const [textPassword, setPassword] = React.useState('');
  const [firstName, setFirstName] = React.useState('');
  const [lastName, setLastName] = React.useState('');
  const [coop, setCoop] = React.useState('');
  const [data, setData] = React.useState('');
  const [code, setCode] = React.useState('');
  const [name, setName] = React.useState('');

  const bottomSheetRef = useRef();

  useEffect(() => {
    axios
      .get(URL.LIST_COOP)
      .then(response => {
        console.log('registerScreen()::then', response.data.data);
        setData(response.data.data);
      })
      .catch(error => {
        console.log('coopListBottomSheet()::error', error);
      });
  }, []);

  const storeData = async (key, value) => {
    try {
      await AsyncStorage.setItem(key, value);
    } catch (e) {
      throw e;
    }
  };

  function _onButtonRegisterPressed() {
    setLoading(true);
    let json = JSON.stringify({
      first_name: firstName,
      last_name: lastName,
      email: email,
      msisdn: phone,
      code_cooperative: code,
      password: textPassword,
    });

    console.log(json);
    fetch(Constant.URL.REGISTER, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: json,
    })
      .then(response => response.json())
      .then(json => {
        let jwtToken = json.jwt;
        storeData(Constant.Key.jwtToken, jwtToken).then(
          r => setLoading(false),
          navigation.push(ScreenName.createOutlet),
        );
      })
      .catch(function (error) {
        console.log(error);
        setLoading(false);
        throw error;
      });
  }

  const onFocusCoop = () => {
    bottomSheetRef.current.open();
  };

  console.log(code, name);

  const onGetDataCoop = (id, name) => {
    console.log('id dari child : ', id);
    console.log('name dari child : ', name);
  };

  return (
    <>
      <SafeAreaView style={{flex: 1}}>
        <StatusBar animated={true} backgroundColor="#D4031D" />
        <TextInput
          onChangeText={value => setName(value)}
          value={name}
          placeholder="Pilih Koperasi"
          mode="outlined"
          dense="false"
          activeOutlineColor="#D4031D"
          onFocus={onFocusCoop}
          style={[margin(16).horizontal, margin(8).vertical]}
        />
        <View
          style={[
            direction.row,
            margin(16).horizontal,
            {justifyContent: 'space-between'},
          ]}>
          <TextInput
            onChangeText={value => setFirstName(value)}
            defaultValue={firstName}
            placeholder={String.firstName}
            mode="outlined"
            dense="false"
            activeOutlineColor="#D4031D"
            style={[margin(8).vertical, {width: '48%'}]}
          />
          <TextInput
            onChangeText={value => setLastName(value)}
            defaultValue={lastName}
            placeholder={String.lastName}
            mode="outlined"
            dense="false"
            activeOutlineColor="#D4031D"
            style={[margin(8).vertical, {width: '48%'}]}
          />
        </View>
        <TextInput
          onChangeText={value => setEmail(value)}
          defaultValue={email}
          placeholder={String.email}
          mode="outlined"
          dense="false"
          activeOutlineColor="#D4031D"
          style={[margin(16).horizontal, margin(8).vertical]}
        />
        <TextInput
          keyboardType="number-pad"
          onChangeText={value => setPhone(value)}
          defaultValue={phone}
          placeholder={String.phoneNumber}
          mode="outlined"
          dense="false"
          activeOutlineColor="#D4031D"
          style={[margin(16).horizontal, margin(8).vertical]}
        />
        <TextInput
          onChangeText={value => setPassword(value)}
          defaultValue={textPassword}
          placeholder={String.password}
          mode="outlined"
          dense="true"
          activeOutlineColor="#D4031D"
          secureTextEntry={true}
          style={[margin(16).horizontal, margin(8).vertical]}
        />

        <Button
          loading={isLoading}
          style={[margin(16).all]}
          onPress={() => _onButtonRegisterPressed()}
          mode="contained"
          color="#D4031D">
          {String.register}
        </Button>

        <TouchableRipple onPress={() => navigation.push('login')}>
          <Text style={[{textAlign: 'center'}, FontStyle.normal]}>
            {String.iHaveAlreadyAccount}
          </Text>
        </TouchableRipple>

        <CoopListBottomSheet
          sheetRef={bottomSheetRef}
          list={data}
          setParentCode={setCode}
          setParentName={setName}
        />
      </SafeAreaView>
    </>
  );
}

const margin = (v?: number) =>
  StyleSheet.create({
    all: {
      margin: v,
    },
    start: {
      marginStart: v,
    },
    end: {
      marginEnd: v,
    },
    vertical: {
      marginTop: v,
      marginBottom: v,
    },
    horizontal: {
      marginStart: v,
      marginEnd: v,
    },
  });
