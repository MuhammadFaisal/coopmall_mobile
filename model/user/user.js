export default class User {
  id: number;
  username: string;
  email: string;
  provider: string;
  confirmed: boolean;
  blocked: boolean;
  createdAt: string;
  updatedAt: string;
  msisdn: string;
  first_name: string;
  last_name: string;
  uuid: string;
  device_id: string;
  device_brand: string;
  device_model: string;
  fcm_token: string;
  ip_address: string;
  coordinate: string;
}
